FROM hoangthienan/docker-fusionpbx-debian

MAINTAINER fuyb <656452024@qq.com>
ENV LANG C.UTF-8
#设置时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone


### 不能直接替换文件， 需要安装完成fusionpbx后替换 ###

# update
# <X-PRE-PROCESS cmd="set" data="global_codec_prefs=G722,PCMU,PCMA,GSM,H263,H264,VP8" />
# <X-PRE-PROCESS cmd="set" data="outbound_codec_prefs=PCMU,PCMA,GSM,H263,H264,VP8" />

# <X-PRE-PROCESS cmd="set" data="default_language=zh" />
# <X-PRE-PROCESS cmd="set" data="default_dialect=cn" />
# <X-PRE-PROCESS cmd="set" data="default_voice=no" />
# <X-PRE-PROCESS cmd="set" data="default_country=CN" />

# <X-PRE-PROCESS cmd="set" data="sound_prefix=$${sounds_dir}/zh/cn/no" />
# ADD conf/vars.xml /etc/freeswitch/vars.xml

# add <X-PRE-PROCESS cmd="include" data="languages/zh/*.xml"/>
# ADD conf/freeswitch.xml /etc/freeswitch/freeswitch.xml

# set rtp-start-port=16384, rtp-end-port=19999
#ADD conf/switch.conf.xml /etc/freeswitch/autoload_configs/switch.conf.xml

# copy chinese languages xml   >>> before install path is /etc/freeswitch/lang after install path is /etc/freeswitch/languages
#ADD conf/lang/zh /etc/freeswitch/lang/zh

#RUN sudo chown -R freeswitch:freeswitch /etc/freeswitch && sudo chmod 777 /etc/freeswitch

# copy chinese sound files
#ADD conf/sounds/zh /usr/share/freeswitch/sounds/zh
#RUN sudo chown -R freeswitch:freeswitch /usr/share/freeswitch/sounds/zh && sudo chmod 777 /usr/share/freeswitch/sounds/zh
